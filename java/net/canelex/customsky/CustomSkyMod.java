package net.canelex.customsky;

import net.canelex.customsky.render.CustomLayer;
import net.canelex.customsky.render.SkyRenderer;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.gui.GuiScreen;
import net.canelex.customsky.gui.GuiScreenListSkies;
import org.lwjgl.input.Keyboard;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraft.client.resources.IResourcePack;
import java.io.File;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import com.google.common.collect.Lists;
import net.minecraftforge.client.IRenderHandler;
import net.canelex.customsky.render.CustomSky;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.Mod;

@Mod(modid = "customskymod", name = "Custom Sky Mod", version = "1.0")
public class CustomSkyMod
{
    private final Minecraft mc;
    private KeyBinding keyOpenSkies;
    public final List<CustomSky> customSkies;
    public int currentSkyID;
    private IRenderHandler render;
    
    public CustomSkyMod() {
        this.mc = Minecraft.getMinecraft();
        this.keyOpenSkies = new KeyBinding("Open Skies Gui", 37, "CustomSky");
        this.customSkies = new ArrayList<>();
        this.currentSkyID = 0;
    }
    
    @Mod.EventHandler
    public void init(final FMLInitializationEvent event) {
        ClientRegistry.registerKeyBinding(this.keyOpenSkies);
        List<IResourcePack> resourcePacks = null;
        resourcePacks = (List<IResourcePack>)ObfuscationReflectionHelper.getPrivateValue((Class)Minecraft.class, (Object)this.mc, new String[] { "defaultResourcePacks", "field_110449_ao" });
        resourcePacks.add((IResourcePack)new SkyResources(new File(this.mc.mcDataDir.getPath())));
        FMLCommonHandler.instance().bus().register((Object)this);
        this.loadSkyResources();
    }
    
    @SubscribeEvent
    public void onKeyInput(final InputEvent.KeyInputEvent event) {
        if (Keyboard.getEventKey() == this.keyOpenSkies.getKeyCode()) {
            this.mc.displayGuiScreen((GuiScreen)new GuiScreenListSkies(this));
            this.loadSkyResources();
        }
    }
    
    @SubscribeEvent
    public void onClientTick(final TickEvent.ClientTickEvent event) {
        if (this.mc.theWorld != null && this.mc.theWorld.provider != null && this.mc.theWorld.provider.getSkyRenderer() == null) {
            this.render = this.mc.theWorld.provider.getSkyRenderer();
            this.mc.theWorld.provider.setSkyRenderer((IRenderHandler)new SkyRenderer(this));
        }
    }
    
    private void loadSkyResources() {
        this.customSkies.clear();
        this.loadDefaultResource("Default Sky I");
        this.loadDefaultResource("Default Sky II");
        this.loadDefaultResource("Default Sky III");
        for (final File skyDir : SkyResources.resourceDir.listFiles()) {
            if (skyDir.isDirectory()) {
                final CustomSky customSky = new CustomSky(skyDir.getName());
                if (new File(skyDir, "dawn.png").exists()) {
                    customSky.dawnLayer = new CustomLayer(skyDir.getName() + "/dawn.png");
                    if (new File(skyDir, "day.png").exists()) {
                        customSky.dayLayer = new CustomLayer(skyDir.getName() + "/day.png");
                        if (new File(skyDir, "dusk.png").exists()) {
                            customSky.duskLayer = new CustomLayer(skyDir.getName() + "/dusk.png");
                            if (new File(skyDir, "night.png").exists()) {
                                customSky.nightLayer = new CustomLayer(skyDir.getName() + "/night.png");
                                this.customSkies.add(customSky);
                            }
                        }
                    }
                }
            }
        }
    }
    
    private void loadDefaultResource(final String resource) {
        final CustomSky customSky = new CustomSky(resource);
        customSky.dawnLayer = new CustomLayer("customskymod", resource + "/dawn.png");
        customSky.dayLayer = new CustomLayer("customskymod", resource + "/day.png");
        customSky.duskLayer = new CustomLayer("customskymod", resource + "/dusk.png");
        customSky.nightLayer = new CustomLayer("customskymod", resource + "/night.png");
        this.customSkies.add(customSky);
    }
}
