package net.canelex.customsky;

import java.awt.image.BufferedImage;
import net.minecraft.client.resources.data.IMetadataSection;
import com.google.common.collect.ImmutableSet;
import java.util.Set;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.InputStream;

import net.minecraft.client.resources.data.MetadataSerializer;
import net.minecraft.util.ResourceLocation;
import java.io.File;
import net.minecraft.client.resources.IResourcePack;

public class SkyResources implements IResourcePack
{
    public static File resourceDir;
    
    public SkyResources(final File mcDataDir) {
        SkyResources.resourceDir = new File(mcDataDir, "skyresources");
        if (!SkyResources.resourceDir.exists()) {
            SkyResources.resourceDir.mkdir();
        }
    }
    
    public InputStream getInputStream(final ResourceLocation resource) throws IOException {
        final File resourceFile = new File(SkyResources.resourceDir, resource.getResourcePath());
        if (resourceFile.exists()) {
            return new FileInputStream(resourceFile);
        }
        return null;
    }
    
    public boolean resourceExists(final ResourceLocation resource) {
        return resource.getResourcePath().contains(".png");
    }
    
    public Set getResourceDomains() {
        return (Set)ImmutableSet.of((Object)"skyresources");
    }

    @Override
    public <T extends IMetadataSection> T getPackMetadata(MetadataSerializer metadataSerializer, String metadataSectionName) throws IOException {
        return null;
    }
    
    public BufferedImage getPackImage() throws IOException {
        return null;
    }
    
    public String getPackName() {
        return "SkyResourcePack";
    }
}
