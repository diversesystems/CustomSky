package net.canelex.customsky.render;

import net.minecraft.client.renderer.VertexBuffer;

import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import org.lwjgl.opengl.GL11;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

public class CustomLayer
{
    public final ResourceLocation resourceLocation;
    
    public CustomLayer(final String resName) {
        this.resourceLocation = new ResourceLocation("skyresources", resName);
    }
    
    public CustomLayer(final String domain, final String resName) {
        this.resourceLocation = new ResourceLocation(domain, resName);
    }
    
    public void renderLayer(final int size) {
        Minecraft.getMinecraft().getTextureManager().bindTexture(this.resourceLocation);
        GlStateManager.alphaFunc(516, 0.1f);
        RenderHelper.disableStandardItemLighting();
        GlStateManager.depthMask(false);
        GlStateManager.disableFog();
        GlStateManager.enableBlend();
        GlStateManager.enableAlpha();
        GlStateManager.enableTexture2D();
        final Tessellator tess = Tessellator.getInstance();
        GlStateManager.pushMatrix();
        GL11.glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
        GL11.glRotatef(-90.0f, 0.0f, 0.0f, 1.0f);
        this.renderSide(tess, 4, size);
        GlStateManager.pushMatrix();
        GL11.glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
        this.renderSide(tess, 1, size);
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix();
        GL11.glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
        this.renderSide(tess, 0, size);
        GlStateManager.popMatrix();
        GL11.glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
        this.renderSide(tess, 5, size);
        GL11.glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
        this.renderSide(tess, 2, size);
        GL11.glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
        this.renderSide(tess, 3, size);
        GlStateManager.popMatrix();
        GlStateManager.depthMask(true);
        GlStateManager.enableFog();
        GlStateManager.disableAlpha();
        GlStateManager.disableBlend();
    }
    
    private void renderSide(final Tessellator tess, final int side, final int size) {
        final VertexBuffer wr = tess.getBuffer();
        final double du = side % 3 / 3.0;
        final double dv = side / 3 / 2.0;
        wr.begin(7, DefaultVertexFormats.POSITION_TEX);
        wr.pos((double)(-size), (double)(-size), (double)(-size)).tex(du, dv).endVertex();
        wr.pos((double)(-size), (double)(-size), (double)size).tex(du, dv + 0.5).endVertex();
        wr.pos((double)size, (double)(-size), (double)size).tex(du + 0.3333333333333333, dv + 0.5).endVertex();
        wr.pos((double)size, (double)(-size), (double)(-size)).tex(du + 0.3333333333333333, dv).endVertex();
        tess.draw();
    }
}
