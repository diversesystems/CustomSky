package net.canelex.customsky.render;

import net.minecraft.client.renderer.VertexBuffer;

import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.multiplayer.WorldClient;
import net.canelex.customsky.CustomSkyMod;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IRenderHandler;

public class SkyRenderer extends IRenderHandler
{
    private final ResourceLocation locationMoonPhasesPng;
    private final ResourceLocation locationSunPng;
    private final ResourceLocation locationCloudsPng;
    private final ResourceLocation locationEndSkyPng;
    private final Minecraft mc;
    private CustomSkyMod mod;
    
    public SkyRenderer(final CustomSkyMod mod) {
        this.locationMoonPhasesPng = new ResourceLocation("textures/environment/moon_phases.png");
        this.locationSunPng = new ResourceLocation("textures/environment/sun.png");
        this.locationCloudsPng = new ResourceLocation("textures/environment/clouds.png");
        this.locationEndSkyPng = new ResourceLocation("textures/environment/end_sky.png");
        this.mc = Minecraft.getMinecraft();
        this.mod = mod;
    }
    
    public void render(final float partialTicks, final WorldClient world, final Minecraft mc) {
        if (mc.theWorld.provider.getDimension() == 1) {
            this.renderSkyEnd();
        }
        else if (mc.theWorld.provider.isSurfaceWorld() && this.mod.customSkies.size() > this.mod.currentSkyID) {
            ((CustomSky)this.mod.customSkies.get(this.mod.currentSkyID)).renderSky();
        }
    }
    
    private void renderSkyEnd() {
        GlStateManager.disableFog();
        GlStateManager.disableAlpha();
        GlStateManager.enableBlend();
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
        RenderHelper.disableStandardItemLighting();
        GlStateManager.depthMask(false);
        this.mc.renderEngine.bindTexture(this.locationEndSkyPng);
        final Tessellator tessellator = Tessellator.getInstance();
        final VertexBuffer worldrenderer = tessellator.getBuffer();
        for (int i = 0; i < 6; ++i) {
            GlStateManager.pushMatrix();
            if (i == 1) {
                GlStateManager.rotate(90.0f, 1.0f, 0.0f, 0.0f);
            }
            if (i == 2) {
                GlStateManager.rotate(-90.0f, 1.0f, 0.0f, 0.0f);
            }
            if (i == 3) {
                GlStateManager.rotate(180.0f, 1.0f, 0.0f, 0.0f);
            }
            if (i == 4) {
                GlStateManager.rotate(90.0f, 0.0f, 0.0f, 1.0f);
            }
            if (i == 5) {
                GlStateManager.rotate(-90.0f, 0.0f, 0.0f, 1.0f);
            }
            worldrenderer.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR);
            worldrenderer.pos(-100.0, -100.0, -100.0).tex(0.0, 0.0).color(40, 40, 40, 255).endVertex();
            worldrenderer.pos(-100.0, -100.0, 100.0).tex(0.0, 16.0).color(40, 40, 40, 255).endVertex();
            worldrenderer.pos(100.0, -100.0, 100.0).tex(16.0, 16.0).color(40, 40, 40, 255).endVertex();
            worldrenderer.pos(100.0, -100.0, -100.0).tex(16.0, 0.0).color(40, 40, 40, 255).endVertex();
            tessellator.draw();
            GlStateManager.popMatrix();
        }
        GlStateManager.depthMask(true);
        GlStateManager.enableTexture2D();
        GlStateManager.enableAlpha();
    }
}
