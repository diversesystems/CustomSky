package net.canelex.customsky.render;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.Minecraft;

public class CustomSky
{
    private final Minecraft mc;
    public CustomLayer dawnLayer;
    public CustomLayer dayLayer;
    public CustomLayer duskLayer;
    public CustomLayer nightLayer;
    public final String skyName;
    private long worldTime;
    private int skyboxSize;
    
    public CustomSky(final String name) {
        this.mc = Minecraft.getMinecraft();
        this.dawnLayer = null;
        this.dayLayer = null;
        this.duskLayer = null;
        this.nightLayer = null;
        this.skyName = name;
    }
    
    public void renderSky() {
        this.worldTime = this.mc.theWorld.getWorldTime() % 24000L;
        this.skyboxSize = this.mc.gameSettings.renderDistanceChunks * 16;
        this.renderLayersOfTime(this.dawnLayer, 0L, 1000L);
        this.renderLayersOfTime(this.dayLayer, 1000L, 12000L);
        this.renderLayersOfTime(this.duskLayer, 12000L, 13000L);
        this.renderLayersOfTime(this.nightLayer, 13000L, 24000L);
    }
    
    private void renderLayersOfTime(final CustomLayer layer, final long thisTime, final long nextTime) {
        float alpha = 0.0f;
        if (this.ticksUntil(this.worldTime, thisTime) > 12000L) {
            if (this.ticksUntil(this.worldTime, nextTime) < 12000L) {
                alpha = 1.0f;
            }
            else if (this.ticksSince(this.worldTime, nextTime) < 200L) {
                alpha = 1.0f - this.ticksSince(this.worldTime, nextTime) / 200.0f;
            }
        }
        else if (this.ticksUntil(this.worldTime, thisTime) < 200L) {
            alpha = 1.0f - this.ticksUntil(this.worldTime, thisTime) / 200.0f;
        }
        if (alpha != 0.0f) {
            GlStateManager.color(1.0f, 1.0f, 1.0f, alpha);
            layer.renderLayer(this.skyboxSize);
        }
    }
    
    private long ticksUntil(final long now, final long next) {
        if (now > next) {
            return next + 24000L - now;
        }
        return next - now;
    }
    
    private long ticksSince(final long now, final long last) {
        if (now > last) {
            return now - last;
        }
        return 24000L + now - last;
    }
}
