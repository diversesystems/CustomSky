package net.canelex.customsky.gui;

import net.canelex.customsky.render.CustomSky;
import java.awt.Color;
import net.canelex.customsky.CustomSkyMod;
import net.minecraft.client.gui.GuiScreen;

public class GuiScreenListSkies extends GuiScreen
{
    private CustomSkyMod mod;
    private boolean clickedScrollBar;
    private int scrollBarY;
    private int scrollBarHeight;
    private int scrollBarAmount;
    private int scrollBoxHeight;
    private int lastY;
    
    public GuiScreenListSkies(final CustomSkyMod mod) {
        this.clickedScrollBar = false;
        this.scrollBarY = 0;
        this.scrollBarHeight = 0;
        this.scrollBarAmount = 0;
        this.scrollBoxHeight = 0;
        this.mod = mod;
    }
    
    public void initGui() {
        this.scrollBoxHeight = this.mod.customSkies.size() * 32;
        this.scrollBarHeight = (this.scrollBoxHeight - (this.scrollBoxHeight - this.height)) * this.height / this.scrollBoxHeight;
    }
    
    public void drawScreen(final int x, final int y, final float partial) {
        drawRect(this.width / 2 - 100, 0, this.width / 2 + 100, this.height, new Color(0, 0, 0, 130).getRGB());
        this.scrollBar(y);
        for (int id = 0; id < this.mod.customSkies.size(); ++id) {
            this.drawSkyResource(id);
        }
    }
    
    protected void mouseClicked(final int x, final int y, final int button) {
        if (x > this.width / 2 + 101 && x <= this.width / 2 + 107 && y >= this.scrollBarY && y <= this.scrollBarY + this.scrollBarHeight) {
            this.clickedScrollBar = true;
        }
        for (int id = 0; id < this.mod.customSkies.size(); ++id) {
            if (x >= this.width / 2 - 99 && x <= this.width / 2 + 99 && y > 5 + id * 32 - this.scrollBarAmount && y < 33 + id * 32 - this.scrollBarAmount) {
                this.mod.currentSkyID = id;
                break;
            }
        }
    }
    
    protected void mouseReleased(final int mouseX, final int mouseY, final int state) {
        this.clickedScrollBar = false;
    }
    
    private void drawSkyResource(final int id) {
        final CustomSky sky = (CustomSky)this.mod.customSkies.get(id);
        if (id == this.mod.currentSkyID) {
            drawRect(this.width / 2 - 100, 4 + id * 32 - this.scrollBarAmount, this.width / 2 + 100, 36 + id * 32 - this.scrollBarAmount, Color.WHITE.getRGB());
        }
        drawRect(this.width / 2 - 99, 5 + id * 32 - this.scrollBarAmount, this.width / 2 + 99, 35 + id * 32 - this.scrollBarAmount, Color.BLACK.getRGB());
        this.drawCenteredString(this.mc.fontRendererObj, ((CustomSky)this.mod.customSkies.get(id)).skyName, this.width / 2, 15 + id * 32 - this.scrollBarAmount, Color.WHITE.getRGB());
    }
    
    private void scrollBar(final int y) {
        if (this.scrollBoxHeight <= this.height) {
            return;
        }
        if (this.clickedScrollBar) {
            this.scrollBarY += y - this.lastY;
            if (this.scrollBarY < 0) {
                this.scrollBarY = 0;
            }
            if (this.scrollBarY + this.scrollBarHeight > this.height) {
                this.scrollBarY = this.height - this.scrollBarHeight;
            }
            this.scrollBarAmount = this.scrollBarY * this.scrollBoxHeight / this.height;
        }
        this.lastY = y;
        drawRect(this.width / 2 + 100, 0, this.width / 2 + 108, this.height, new Color(0, 0, 0, 170).getRGB());
        drawRect(this.width / 2 + 101, this.scrollBarY, this.width / 2 + 107, this.scrollBarY + this.scrollBarHeight, Color.WHITE.getRGB());
    }
}
